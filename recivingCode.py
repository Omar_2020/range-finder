from raspi_lora import LoRa, ModemConfig
import time
import queue

lora = LoRa(0, 0, 22, modem_config=ModemConfig.Bw125Cr48Sf4096,
tx_power=20, freq=915, acks=True, receive_all=True, crypto=None)

def on_recv(payload):
    payload_queue.put(payload)
def process_payload(payload):
    print(payload.rssi)


lora.on_recv = on_recv
payload_queue = queue.Queue()
lora.set_mode_rx()
time.sleep(1)

print("ready to recive!")
while True:
    
    try:
        process_payload(payload_queue.get_nowait())
    except queue.Empty:
        time.sleep(1)

lora.close()