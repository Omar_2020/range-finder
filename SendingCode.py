from raspi_lora import LoRa, ModemConfig
import time
import queue

lora = LoRa(0, 0, 22, modem_config=ModemConfig.Bw125Cr48Sf4096,
tx_power=20, freq=915, acks=True, receive_all=True, crypto=None)

lora.set_mode_tx()

while True:
    if lora.send("!", 64, 14, 32) == True:
        print("Message sent!")
    time.sleep(1)

lora.close()
